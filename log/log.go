package log

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"go.uber.org/zap"
)

var (
	logFile = flag.String("log", "./logs", "default log file")
)

// InitLogger ... 初始化log服务和加载配置文件
func InitLogger() *zap.Logger {

	var logfiledir = *logFile

	// See the documentation for Config and zapcore.EncoderConfig for all the
	err := os.MkdirAll(logfiledir, os.ModePerm)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("create log dir:", logfiledir, " failed :", err)
			os.Exit(1)
		}

		if os.IsPermission(err) {
			fmt.Println("create log dir:", logfiledir, " failed by permission :", err)
			os.Exit(1)
		}
	}

	Shanghai, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		log.Printf("Unable to load timezones: %s\n", err)
	} else {
		log.Printf("Successfully loaded %q\n", Shanghai)
	}

	logcfg := zap.NewProductionConfig()
	logcfg.OutputPaths = append(logcfg.OutputPaths, path.Join(logfiledir, fmt.Sprintf("log_%s.log", time.Now().Format("20060102_1504"))))

	logger, err := logcfg.Build()
	if err != nil {
		panic(err)
	}

	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)

	//defer logger.Sync()
	log.SetFlags(0)

	logger.Info(fmt.Sprintf("path:%s, %s", filepath.Dir(os.Args[0]), os.Args[0]))

	return logger
}
