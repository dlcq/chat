package event_bus

import (
	"testing"
)

func TestBus(t *testing.T) {
	ch := make(EventCh)
	Bus.Subscribe("1", ch)
	Bus.Subscribe("1", ch)
	Bus.Unsubscribe("1", ch)
}
