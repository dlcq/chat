package scheduler

import (
	"context"
	"sync"
)

type Scheduler interface {
	Push(Job)
	Wait() (success int, failed int)
	Stop()
}

type scheduler struct {
	ctx     context.Context
	cancel  context.CancelFunc
	nJobs   int
	nErrors int
	jobs    chan Job
	errors  chan error
	wg      *sync.WaitGroup
}

func NewScheduler(workers int, bufferSize int) Scheduler {
	ctx, cancel := context.WithCancel(context.Background())
	var jobs chan Job
	if bufferSize > 0 {
		jobs = make(chan Job, bufferSize)
	} else {
		jobs = make(chan Job)
	}
	errors := make(chan error)
	wg := new(sync.WaitGroup)
	s := &scheduler{
		ctx:    ctx,
		cancel: cancel,
		jobs:   jobs,
		errors: errors,
		wg:     wg,
	}
	for i := 0; i < workers; i++ {
		NewWorker(ctx, jobs, errors, wg).Start()
	}
	go func() {
		for {
			err, ok := <-s.errors
			if !ok {
				return
			}
			if err != nil {
				s.nErrors++
			}
		}
	}()
	return s
}

func (s *scheduler) Push(job Job) {
	s.jobs <- job
	s.nJobs++
	s.wg.Add(1)
}

func (s *scheduler) Stop() {
	s.cancel()

	close(s.jobs)
	close(s.errors)
}

func (s *scheduler) Wait() (int, int) {
	s.wg.Wait()
	close(s.jobs)
	close(s.errors)

	return s.nJobs - s.nErrors, s.nErrors
}
