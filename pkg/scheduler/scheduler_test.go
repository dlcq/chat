package scheduler

import (
	"errors"
	"math/rand"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) {
	d := NewScheduler(3, 3)
	for i := 0; i < 100; i++ {
		d.Push(NewSimpleJob(func() error {
			time.Sleep(time.Millisecond * 20)
			if rand.Intn(3) == 0 {
				return errors.New("simple error")
			}
			return nil
		}))
	}
	t.Log(d.Wait())
}
