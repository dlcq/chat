package scheduler

import (
	"context"
	"sync"
)

type Worker interface {
	Start()
}

func NewWorker(ctx context.Context, jobs chan Job, errors chan error, wg *sync.WaitGroup) Worker {
	return &worker{
		ctx:    ctx,
		jobs:   jobs,
		errors: errors,
		wg:     wg,
	}
}

type worker struct {
	ctx    context.Context
	jobs   chan Job
	errors chan error
	wg     *sync.WaitGroup
}

func (w *worker) Start() {
	go func() {
		for {
			select {
			case <-w.ctx.Done():
				return
			case job, ok := <-w.jobs:
				if !ok {
					return
				}
				w.errors <- job.Run()
				w.wg.Done()
			}
		}
	}()
}
