package scheduler

type Job interface {
	Run() error
}

type funcJob func() error

func (f funcJob) Run() error {
	return f()
}

func NewSimpleJob(f func() error) Job {
	return funcJob(f)
}