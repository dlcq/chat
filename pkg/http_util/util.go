package http_util

import (
	"encoding/json"
	"net/http"
	"net/url"
)

type Context interface {
	Json(interface{})
	Error(string)
	BindJson(interface{}) error
	Authorization() string
}

func (c *filter) Json(v interface{}) {
	c.response.WriteHeader(http.StatusOK)
	c.response.Header().Add("Content-Type", "application/json")
	err := json.NewEncoder(c.response).Encode(v)
	if err != nil {
		panic(err)
	}
}

func (c *filter) Error(msg string) {
	http.Error(c.response, msg, http.StatusBadRequest)
}

func (c *filter) BindJson(msg interface{}) error {
	return json.NewDecoder(c.request.Body).Decode(msg)
}

func (c *filter) Authorization() string {
	auth := c.request.Header.Get("Authorization")
	esAuth, err := url.PathUnescape(auth)
	if err != nil {
		return auth
	}
	return esAuth
}

func setHeader(w http.ResponseWriter) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
	w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
	w.Header().Add("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
	w.Header().Add("Access-Control-Allow-Credentials", "true")
}

type HandleFunc func(Context)

type filter struct {
	request  *http.Request
	response http.ResponseWriter
	handlers []HandleFunc
}

func NewFilter(handlers ...HandleFunc) *filter {
	return &filter{handlers: handlers}
}

func (c *filter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.request = r
	c.response = w

	setHeader(w)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	for _, fn := range c.handlers {
		fn(c)
	}
}
