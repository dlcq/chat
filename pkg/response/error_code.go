package response

//go:generate stringer -type ErrCode -linecomment -output error_string.go
type ErrCode int

const (
	/*error code*/
	SuccessCode   ErrCode = 0     // 成功
	ErrSystemCode ErrCode = 50000 //内部错误
	ErrParamCode  ErrCode = 50001 //输入参数错误
)
