package response

import (
	"encoding/json"
	"reflect"
)

type ReturnJSON struct {
	Code int         //返回错误号
	Desc string      //错误描述
	Data interface{} //返回参数
}

func Build(code ErrCode, data interface{}) string {
	ret := new(ReturnJSON)
	ret.Code = int(code)
	ret.Desc = code.String()
	if IsNil(data) {
		ret.Data = struct{}{}
	} else {
		ret.Data = data
	}

	retByte, _ := json.Marshal(ret)
	return string(retByte)
}

func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}
	vi := reflect.ValueOf(i)
	if vi.Kind() == reflect.Ptr {
		return vi.IsNil()
	}
	return false
}
