package safe_ring

import (
	"container/ring"
	"sync"
)

var (
	SafeRing *safeRing
)

type safeRing struct {
	mu sync.RWMutex
	r  *ring.Ring
}

func init() {
	SafeRing = new(safeRing)
}

func (sr *safeRing) Link(v interface{}) {
	sr.mu.Lock()
	defer sr.mu.Unlock()

	node := ring.New(1)
	node.Value = v

	if sr.r == nil {
		sr.r = node
	} else {
		sr.r.Link(node)
	}
}

func (sr *safeRing) Unlink() {
	sr.mu.Lock()
	defer sr.mu.Unlock()
	if sr.r == nil {
		return
	}

	if sr.r.Next() != sr.r {
		sr.r = sr.r.Prev()
		sr.r.Unlink(1)
		return
	}
	
	sr.r = nil
}

func (sr *safeRing) Prev() interface{} {
	sr.mu.RLock()
	defer sr.mu.RUnlock()
	if sr.r == nil {
		return nil
	}
	
	sr.r = sr.r.Prev()
	return sr.r.Value
}

func (sr *safeRing) Next() interface{} {
	sr.mu.RLock()
	defer sr.mu.RUnlock()

	if sr.r == nil {
		return nil
	}

	sr.r = sr.r.Next()
	return sr.r.Value
}
