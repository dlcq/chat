package config

import (
	"errors"
	"flag"
	"github.com/BurntSushi/toml"
	"os"
	"reflect"
	"strconv"
)

var (
	// 配置文件地址
	configFile = flag.String("config", "conf.toml", "default config file")
)

// ParseToml 合并配置文件
func ParseToml(cfg interface{}) error {
	m := map[string]interface{}{}
	_, err := toml.DecodeFile(*configFile, m)
	if errors.Is(err, os.ErrNotExist) {
		return nil
	}

	mergeToml(cfg, m)
	return nil
}

func mergeToml(cfg interface{}, m map[string]interface{}) {
	val := reflect.ValueOf(cfg).Elem()
	typ := val.Type()
	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)

		// Recursively resolve embedded types.
		if field.Anonymous {
			var fieldPtr reflect.Value
			switch val.FieldByName(field.Name).Kind() {
			case reflect.Struct:
				fieldPtr = val.FieldByName(field.Name).Addr()
			case reflect.Ptr:
				fieldPtr = reflect.Indirect(val).FieldByName(field.Name)
			}
			if !fieldPtr.IsNil() {
				mergeToml(fieldPtr.Interface(), m)
				continue
			}
		}

		fieldVal := val.FieldByName(field.Name)
		if tag, ok := field.Tag.Lookup("toml"); ok {
			if cfgValue, exist := m[tag]; exist {
				fieldVal.Set(reflect.ValueOf(cfgValue))
			}
		}

		if envName, ok := field.Tag.Lookup("env"); ok {
			if envValue, exist := os.LookupEnv(envName); exist {
				switch fieldVal.Kind() {
				case reflect.Int, reflect.Int32, reflect.Int64, reflect.Int16, reflect.Int8:
					fieldVal.SetInt(toInt(envValue))
				case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
					fieldVal.SetUint(toUint(envValue))
				case reflect.Float32, reflect.Float64:
					fieldVal.SetFloat(toFloat(envValue))
				case reflect.String:
					fieldVal.SetString(envValue)
				case reflect.Bool:
					fieldVal.SetBool(toBool(envValue))
				}
			}
		}
	}
}

func toInt(v string) int64 {
	i, _ := strconv.ParseInt(v, 10, 64)
	return i
}

func toUint(v string) uint64 {
	i, _ := strconv.ParseUint(v, 10, 64)
	return i
}

func toFloat(v string) float64 {
	f, _ := strconv.ParseFloat(v, 64)
	return f
}

func toBool(v string) bool {
	b, _ := strconv.ParseBool(v)
	return b
}
