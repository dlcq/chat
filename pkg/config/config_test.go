package config

import "testing"

func TestGet(t *testing.T) {
	type cfg struct {
		Port int64 `toml:"port" env:"ChocolateyLastPathUpdate"`
	}

	c := &cfg{}
	mergeToml(c, map[string]interface{}{})
	t.Log(c)
}
