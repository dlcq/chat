package oauth

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"math/big"
	"sync"
	"time"
)

var codeManager CodeManager

type CodeInfo struct {
	AccessToken  string      `json:"access_token"`
	TokenType    string      `json:"token_type"`
	ExpiresIn    int64       `json:"expires_in"`
	RefreshToken string      `json:"refresh_token"`
	Scope        string      `json:"scope"`
	Uid          interface{} `json:"uid"`
	Info         interface{} `json:"info"`
}

type CodeManager interface {
	GenCode(clientID string, uid interface{}) (string, error)
	GenToken(code string) (string, error)
	Validate(token string) (*CodeInfo, error)
	RefreshToken(oldToken string) (string, error)
}

type simpleCodeManager struct {
	codeAssignInfo map[string][]struct {
		clientID string
		uid      interface{}
		code     string
	} // clientID, uid -> code
	tokenAssignInfo map[string][]struct {
		clientID string
		uid      interface{}
		token    string
	}                                  // clientID, uid -> token
	tokenCodeInfo map[string]*CodeInfo // token -> *CodeInfo
	mu            *sync.RWMutex
}

func (cm *simpleCodeManager) GenCode(clientID string, uid interface{}) (string, error) {
	h := md5.New()
	err := json.NewEncoder(h).Encode(uid)
	if err != nil {
		return "", err
	}
	h.Write([]byte(clientID))
	key := hex.EncodeToString(h.Sum(nil))
	code := hex.EncodeToString(h.Sum(big.NewInt(time.Now().UnixNano()).Bytes()))

	cm.mu.Lock()
	defer cm.mu.Unlock()

	codes, ok := cm.codeAssignInfo[key]
	if ok {
		for _, code := range codes {
			if code.uid == uid && code.clientID == clientID {
				return code.code, nil
			}
		}
	}

	cm.codeAssignInfo[key] = append(cm.codeAssignInfo[key], struct {
		clientID string
		uid      interface{}
		code     string
	}{clientID: clientID, uid: uid, code: code})

	return code, nil
}