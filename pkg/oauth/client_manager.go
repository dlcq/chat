package oauth

import "errors"

var (
	ClientManagerNotExists = errors.New("client manager not exists")
	ClientAlreadyExists    = errors.New("client already exists")
	ClientNotExists        = errors.New("client not exists")
)

var clientManager ClientManager = &simpleClientManager{clients: make(map[string]string)}

type ClientManager interface {
	GetClient(string) (string, error)
	AddClient(string, string) error
}

func SetClientManager(cm ClientManager) {
	clientManager = cm
}

func GetClient(clientID string) (string, error) {
	if clientManager == nil {
		return "", ClientManagerNotExists
	}
	return clientManager.GetClient(clientID)
}

func AddClient(clientID, secrete string) error {
	if clientManager == nil {
		return ClientManagerNotExists
	}
	return clientManager.AddClient(clientID, secrete)
}

type simpleClientManager struct {
	clients map[string]string
}

func (cm *simpleClientManager) AddClient(clientID, secrete string) error {
	if cm == nil || cm.clients == nil {
		return ClientManagerNotExists
	}
	if _, ok := cm.clients[clientID]; ok {
		return ClientAlreadyExists
	}
	cm.clients[clientID] = secrete
	return nil
}

func (cm *simpleClientManager) GetClient(clientId string) (string, error) {
	if cm == nil || cm.clients == nil {
		return "", ClientManagerNotExists
	}
	secrete, ok := cm.clients[clientId]
	if !ok {
		return "", ClientNotExists
	}
	return secrete, nil
}
