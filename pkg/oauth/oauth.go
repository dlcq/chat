package oauth

import (
	"log"
	"net/http"
)

type ResponseType string

const (
	AuthorizationCode ResponseType = "code"               // 授权码
	Implicit          ResponseType = "token"              // 隐藏式
	Password          ResponseType = "password"           // 密码式
	ClientCredentials ResponseType = "client_credentials" // 客户端凭证
)

func Authorize(t ResponseType, clientID string, redirect string, scope string) (int, []byte) {
	switch t {
	case AuthorizationCode:
	case Implicit:
	case Password:
	case ClientCredentials:
	default:
		log.Println("unsupported Authorize type ", t)
	}
	return http.StatusMethodNotAllowed, nil
}
