package pack

import (
	"encoding/json"
	"io"
)

func Pack(w io.Writer, msg interface{}) error {
	return json.NewEncoder(w).Encode(msg)
}

func Unpack(r io.Reader, msg interface{}) error {
	return json.NewDecoder(r).Decode(msg)
}
