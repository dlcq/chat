package main

import (
	"chat/app"
	"chat/internal/server"
	"chat/log"
	"chat/pkg/config"
	"flag"
	"fmt"
	"go.uber.org/zap"
	"os"
)

func main() {
	flag.Parse()

	log.InitLogger()
	zap.L().Info("chat-server start...")

	zap.L().Info(showVersion())

	cfg := newConfig()
	err := config.ParseToml(cfg)
	if err != nil {
		zap.L().Info("chat-server start", zap.String("err", err.Error()))
		os.Exit(-1)
	}

	service := server.New(fmt.Sprintf("%s:%d", cfg.BindAddr, cfg.Port))

	zap.L().Info("chat-server start at", zap.String("addr", cfg.BindAddr), zap.Int("port", cfg.Port))

	err = service.Start()
	if err != nil {
		zap.L().Info("chat-server start", zap.String("err", err.Error()))
		os.Exit(-1)
	}

	app.WaitingForExitSignal()

	err = service.Stop()
	if err != nil {
		zap.L().Info("chat-server stop", zap.String("err", err.Error()))
		os.Exit(-1)
	}

	zap.L().Info("chat-server stop...")
}
