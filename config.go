package main

type Config struct {
	BindAddr string `toml:"bind_addr" env:"BIND_ADDR"`
	Port     int    `toml:"port" env:"PORT"`
}

// newConfig return the default config
func newConfig() *Config {
	return &Config{
		BindAddr: "0.0.0.0",
		Port:     8080,
	}
}
