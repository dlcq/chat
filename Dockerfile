FROM hub-mirror.c.163.com/library/golang:alpine AS builder

WORKDIR /go/src/app
COPY ./ .

ENV GO111MODULE on
ENV GOPROXY https://goproxy.cn,direct

RUN go build -ldflags "-X 'main._buildTime=$(date +'%Y.%m.%d.%H%M%S')' -X 'main._buildVersion=$(go version)'"
RUN cp chat /go/bin/chat

FROM hub-mirror.c.163.com/library/alpine

WORKDIR /app

COPY --from=builder /go/bin/chat .

EXPOSE 8080
ENTRYPOINT ["./chat"]