package app

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

// ShowUsage ...
func ShowUsage(usage func()) {
	if usage != nil {
		usage()
	}

	fmt.Printf("%v -configs ./etc					app config files dir \r\n", filepath.Base(os.Args[0]))
	fmt.Printf("%v -log ./logs						app log file \r\n", filepath.Base(os.Args[0]))

	os.Exit(0)
}

// WaitingForExitSignal ...
func WaitingForExitSignal() {

	var iskill = false
	c := make(chan os.Signal)                                                                                                  //监听指定信号 ctrl+c kill
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.Signal(10), syscall.Signal(12)) //syscall.SIGUSR1, syscall.SIGUSR2)

	for {
		sig := <-c

		switch sig {
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
			log.Println("Signal exit:", sig)
			iskill = true
			break
		case syscall.Signal(10): //syscall.SIGUSR1: //kill -10 pid
			log.Println("Signal usr1:", sig)
			iskill = true
			break
		case syscall.Signal(12): //syscall.SIGUSR2: //kill -12 pid
			log.Println("Signal usr2:", sig)
			break
		default:
			log.Println("Signal other:", sig)
		}

		if iskill {
			log.Println("app exit by signal:", sig)
			break
		}

		log.Println("app got signal:", sig)
	}

	signal.Stop(c)
}
