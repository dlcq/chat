package message

import (
	"chat/pkg/event_bus"
	"errors"
)

type Type int

const (
	TEXT Type = iota
	IMAGE
	AUDIO
	VIDEO
	FILE
	STRUCT
	ERROR
)

var (
	TimeOut = errors.New("send message time out")
)

type Message struct {
	T    Type   `json:"t,omitempty"`
	Body string `json:"body,omitempty"`
	Room string `json:"room,omitempty"`
	User string `json:"user,omitempty"`
}

func Send(body string, t Type, room string, user string) error {
	return send(Message{
		T:    t,
		Body: body,
		Room: room,
		User: user,
	})
}

func send(msg Message) error {
	go event_bus.Bus.Publish(msg.Room, &msg)
	return nil
}

func NewErrorMsg(s string) *Message {
	return newMsg(ERROR, s)
}

func NewStructMsg(msg string) *Message {
	return newMsg(STRUCT, msg)
}

func NewTextMsg(msg string) *Message {
	return newMsg(TEXT, msg)
}

func newMsg(t Type, msg string) *Message {
	return &Message{
		T:    t,
		Body: msg,
	}
}
