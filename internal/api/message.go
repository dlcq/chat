package api

var (
	ErrorParam = "参数错误"
)

type LoginReq struct {
	UserName string `json:"user_name"`
}

type CreateRoomReq struct {
	RoomName string `json:"room_name"`
	RoomPwd  string `json:"room_pwd"`
}

type EnterRoomReq struct {
	RoomName string `json:"room_name"`
	RoomPwd  string `json:"room_pwd"`
}

type RoomUserReq struct {
	RoomName string `json:"room_name"`
}
