package server

import (
	"chat/internal/api"
	"chat/internal/message"
	"chat/internal/room"
	"chat/internal/user"
	"chat/pkg/http_util"
	"chat/pkg/pack"
	"golang.org/x/net/websocket"
	"net/http"
	"net/url"
)

type Server interface {
	Start() error
	Stop() error
}

func New(addr string) Server {
	return &server{addr: addr}
}

type server struct {
	addr string
}

func (s *server) Start() error {
	http.Handle("/ws", websocket.Handler(ws))
	http.Handle("/login", http_util.NewFilter(login))
	http.Handle("/rooms", http_util.NewFilter(listRooms))
	http.Handle("/roomUsers", http_util.NewFilter(listRoomUsers))
	http.Handle("/enterRoom", http_util.NewFilter(enterRoom))
	http.Handle("/createRoom", http_util.NewFilter(createRoom))
	go http.ListenAndServe(s.addr, nil)
	return nil
}

func (s *server) Stop() error {
	return nil
}

func ws(w *websocket.Conn) {
	userName := w.Request().Header.Get("Sec-WebSocket-Protocol")
	userName, _ = url.QueryUnescape(userName) // handle 中文

	currentUser, err := user.Get(userName)
	if err != nil {
		pack.Pack(w, message.NewErrorMsg(err.Error()))
		return
	}

	defer currentUser.Logout()
	err = currentUser.Reset(w)
	if err != nil {
		pack.Pack(w, message.NewErrorMsg(err.Error()))
		return
	}

	for {
		var msg = new(message.Message)
		err = pack.Unpack(w, msg)
		if err != nil {
			pack.Pack(w, message.NewErrorMsg(err.Error()))
			return
		}
		err = currentUser.Send(msg.T, msg.Body)
		if err != nil {
			pack.Pack(w, message.NewErrorMsg(err.Error()))
			return
		}
	}
}

func login(c http_util.Context) {
	req := &api.LoginReq{}
	err := c.BindJson(req)
	if err != nil {
		c.Error(api.ErrorParam)
		return
	}
	if len(req.UserName) > 20 { // 限制用户名最长20
		c.Error(api.ErrorParam)
		return
	}

	_, err = user.New(req.UserName)
	if err != nil {
		c.Error(err.Error())
		return
	}
	c.Json(struct{}{})
}

func createRoom(c http_util.Context) {
	userName := c.Authorization()
	req := &api.CreateRoomReq{}
	err := c.BindJson(req)
	if err != nil {
		c.Error(api.ErrorParam)
		return
	}

	u, err := user.Get(userName)
	if err != nil {
		c.Error(err.Error())
		return
	}

	_, err = room.New(req.RoomName, req.RoomPwd)
	if err != nil {
		c.Error(err.Error())
		return
	}
	err = u.Enter(req.RoomName, req.RoomPwd)
	if err != nil {
		c.Error(err.Error())
		return
	}
	c.Json(struct{}{})
}

func listRooms(c http_util.Context) {
	rooms := room.List()
	result := make([]struct {
		Name   string `json:"name"`
		Locked bool   `json:"locked"`
	}, 0)
	for _, room := range rooms {
		result = append(result, struct {
			Name   string `json:"name"`
			Locked bool   `json:"locked"`
		}{Name: room.Name(), Locked: room.Locked()})
	}

	c.Json(result)
}

func enterRoom(c http_util.Context) {
	req := &api.EnterRoomReq{}
	err := c.BindJson(req)
	if err != nil {
		c.Error(api.ErrorParam)
		return
	}

	userName := c.Authorization()
	u, err := user.Get(userName)
	if err != nil {
		c.Error(err.Error())
		return
	}

	err = u.Enter(req.RoomName, req.RoomPwd)
	if err != nil {
		c.Error(err.Error())
		return
	}
	c.Json(struct{}{})
}

func listRoomUsers(c http_util.Context) {
	req := &api.RoomUserReq{}
	err := c.BindJson(req)
	if err != nil {
		c.Error(api.ErrorParam)
		return
	}

	if len(req.RoomName) == 0 {
		c.Json(struct{}{})
		return
	}
	r, err := room.Get(req.RoomName)
	if err != nil {
		c.Error(err.Error())
		return
	}
	us := r.Users()
	c.Json(us)
	return
}
