package user

import (
	"chat/internal/message"
	"chat/internal/room"
	"chat/pkg/event_bus"
	"chat/pkg/pack"
	"errors"
	"github.com/satori/go.uuid"
	"golang.org/x/net/websocket"
	"sort"
	"strings"
	"sync"
)

var (
	users = new(sync.Map)
)

// errors
var (
	Exist       = errors.New("该用户名已存在")
	Offline     = errors.New("该用户已下线")
	InRoom      = errors.New("已经在房间中")
	OutRoom     = errors.New("不在房间中")
	NotExist    = errors.New("该用户不存在")
	ErrParam    = errors.New("参数错误")
	AlreadyConn = errors.New("该用户已连接")
)

type User interface {
	Send(message.Type, string) error
	Recv() (*message.Message, error)
	Logout()
	Leave() error
	Enter(string, string) error
	Reset(*websocket.Conn) error
	Name() string
}

func New(name string) (User, error) {
	if len(name) == 0 {
		return nil, ErrParam
	}
	if _, ok := users.Load(name); ok {
		return nil, Exist
	}

	recv := make(event_bus.EventCh)
	rand := uuid.NewV4()
	token := strings.ReplaceAll(rand.String(), "-", "")

	u := &user{name: name, recv: recv, token: token}
	go u.serve()
	users.Store(name, u)
	return u, nil
}

func List() []User {
	list := make([]User, 0)
	users.Range(func(key, value interface{}) bool {
		list = append(list, value.(*user))
		return true
	})
	sort.Slice(list, func(i, j int) bool {
		return list[i].Name() <= list[j].Name()
	})

	return list
}

func Get(name string) (User, error) {
	u, ok := users.Load(name)
	if !ok {
		return nil, NotExist
	}
	return u.(*user), nil
}

type user struct {
	mu    sync.RWMutex
	name  string
	room  string
	token string
	conn  *websocket.Conn
	recv  event_bus.EventCh
}

func (u *user) Send(t message.Type, body string) error {
	u.mu.RLock()
	defer u.mu.RUnlock()

	if u == nil {
		return Offline
	}
	if len(u.room) == 0 {
		return nil
	}
	return message.Send(body, t, u.room, u.name)
}

func (u *user) Recv() (*message.Message, error) {
	msg, ok := <-u.recv
	if !ok {
		return nil, Offline
	}
	return (msg.Data).(*message.Message), nil
}

func (u *user) Enter(name string, pwd string) error {
	if len(name) <= 0 {
		return ErrParam
	}

	u.mu.Lock()
	defer u.mu.Unlock()

	if len(u.room) > 0 {
		if u.room == name {
			return nil
		}
		err := u.leave()
		if err != nil {
			return err
		}
	}

	r, err := room.Get(name)
	if err != nil {
		return err
	}
	if !r.Verify(pwd) {
		return room.ErrPwd
	}
	u.room = name
	r.Add(u.name)

	event_bus.Bus.Subscribe(name, u.recv)
	return nil
}

func (u *user) Leave() error {
	u.mu.Lock()
	defer u.mu.Unlock()
	return u.leave()
}

func (u *user) Logout() {
	u.mu.Lock()
	defer u.mu.Unlock()

	u.leave()

	u.conn = nil
	if u.recv != nil {
		close(u.recv)
		u.recv = nil
	}

	users.Delete(u.name)
}

func (u *user) Reset(conn *websocket.Conn) error {
	u.mu.Lock()
	defer u.mu.Unlock()

	if u.conn != nil {
		return AlreadyConn
	}
	u.conn = conn
	return nil
}

func (u *user) Name() string {
	return u.name
}

func (u *user) serve() {
	for {
		msg, err := u.Recv()
		if err != nil {
			return
		}
		u.mu.RLock()
		if u.conn != nil {
			pack.Pack(u.conn, msg)
		}
		u.mu.RUnlock()
	}
}

func (u *user) leave() error {
	if len(u.room) == 0 {
		return nil
	}

	r, err := room.Get(u.room)
	if err != nil {
		return err
	}
	r.Leave(u.name)

	event_bus.Bus.Unsubscribe(u.room, u.recv)
	u.room = ""
	return nil
}
