package room

import (
	"errors"
	"sort"
	"sync"
)

var (
	rooms = new(sync.Map)
)

var (
	Exist    = errors.New("该房间已存在")
	NotExist = errors.New("该房间不存在")
	ErrPwd   = errors.New("房间密码错误")
	ErrParam = errors.New("参数错误")
)

type Room interface {
	Verify(string) bool
	Add(string)
	Leave(string)
	Name() string
	Locked() bool
	Users() []string
}

func New(name string, pwd string) (Room, error) {
	if len(name) == 0 {
		return nil, ErrParam
	}
	if _, ok := rooms.Load(name); ok {
		return nil, Exist
	}
	r := &room{
		name: name,
		pwd:  pwd,
	}
	rooms.Store(name, r)
	return r, nil
}

func Get(name string) (Room, error) {
	r, ok := rooms.Load(name)
	if !ok {
		return nil, NotExist
	}
	return r.(*room), nil
}

func List() []Room {
	list := make([]Room, 0)
	rooms.Range(func(key, value interface{}) bool {
		list = append(list, value.(*room))
		return true
	})
	sort.Slice(list, func(i, j int) bool {
		return list[i].Name() <= list[j].Name()
	})

	return list
}

type room struct {
	name  string
	pwd   string
	users []string
	mu    sync.RWMutex
}

func (r *room) Verify(pwd string) bool {
	if r.pwd == pwd {
		return true
	}
	return false
}

func (r *room) List() []string {
	return r.users
}

func (r *room) Add(user string) {
	if len(user) == 0 {
		return
	}
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, u := range r.users {
		if u == user {
			return
		}
	}
	r.users = append(r.users, user)
}

func (r *room) Leave(user string) {
	if len(user) == 0 {
		return
	}

	r.mu.Lock()
	defer r.mu.Unlock()

	for i, u := range r.users {
		if u == user {
			r.users = append(r.users[:i], r.users[i+1:]...)
			if len(r.users) == 0 {
				rooms.Delete(r.name)
			}
			return
		}
	}
}

func (r *room) Name() string {
	return r.name
}

func (r *room) Locked() bool {
	return len(r.pwd) > 0
}

func (r *room) Users() []string {
	return r.users
}
