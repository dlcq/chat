package main

import (
	"fmt"
	"net/http"
)

var (
	_buildTime    string
	_buildVersion string
)

func showVersion() string {
	return fmt.Sprintf("_buildTime: %s, _buildVersion: %s", _buildTime, _buildVersion)
}

func init() {
	http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
		output := showVersion()
		w.Write([]byte(output))
	})
}
